<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('kpsi/public/admin','AdminController@index');

//untuk mhs
Route::get('kpsi/public/mhs','MhsController@index');
Route::get('kpsi/public/mhs/suratketerangan','MhsController@suratketerangan');
Route::post('kpsi/public/mhs/suratketeranganpost','MhsController@suratketeranganpost');
Route::get('kpsi/public/mhs/prakp','MhsController@praKp');
Route::post('kpsi/public/mhs/prakppost','MhsController@praKpPost');
Route::get('kpsi/public/mhs/kp','MhsController@kp');
Route::post('kpsi/public/mhs/kppost','MhsController@kpPost');
Route::post('kpsi/public/mhs/kpupdate','MhsController@kpUpdate');

//untuk dosen
Route::get('kpsi/public/dosen','DosenController@index');
Route::get('kpsi/public/dosen/suratketerangan','DosenController@suratKeterangan');
Route::post('kpsi/public/dosesn/suratketeranganpost','DosenController@suratKeteranganPost');
Route::get('kpsi/public/dosen/suratketerangan/{idSurat}/{keputusan}','DosenController@suratKeteranganVerif');
Route::get('kpsi/public/dosen/kpprakp','DosenController@kpPraKp');
Route::post('kpsi/public/dosen/prakpverif','DosenController@praKpVerif');
Route::post('kpsi/public/dosen/kpverif','DosenController@kpVerif');
Route::get('kpsi/public/dosen/ujiankp','DosenController@ujianKp');
Route::get('kpsi/public/dosen/listujiankp','DosenController@listUjianKp');
Route::post('kpsi/public/dosen/updateujiankp','DosenController@updateUjianKp');
Route::get('kpsi/public/dosen/periodekp','DosenController@periodeKp');
Route::post('kpsi/public/dosen/updateperiodekp','DosenController@updatePeriodeKp');
Route::post('kpsi/public/dosen/periodekppost','DosenController@periodeKpPost');

//login google
Route::get('kpsi/public/google', function () {
    return view('googleAuth');
});
    
Route::get('kpsi/public/auth/google', 'Auth\LoginController@redirectToGoogle');
Route::get('kpsi/public/auth/googlelogout', 'MhsController@logOutGoogle');
Route::get('kpsi/public/auth/google/callback', 'Auth\LoginController@handleGoogleCallback');