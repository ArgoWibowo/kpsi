<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
    	'kpsi/public/dosen/kpverif',
    	'kpsi/public/dosen/prakpverif',
        'kpsi/public/dosen/updateujiankp',
        'kpsi/public/dosen/updateperiodekp'
    ];
}
