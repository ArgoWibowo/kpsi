<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Auth;
use Exception;
use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/kpsi/public';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function logOutGoogle(Request $request){
        Auth::logout();
        $request->session()->forget('login');
        return redirect('/kpsi/public/google');
    }
   
    public function handleGoogleCallback(Request $request)
    {
        try {
            $user = Socialite::driver('google')->user();
            //dd($user);

            //check if email is valid SI email or not
            //0 = false, 1 = dosen, 2 = mhs
            $is_valid = 0;

            if(str_contains($user->email,'@si.ukdw.ac.id')){
                $is_valid = 2;
            }elseif(str_contains($user->email,'@staff.ukdw.ac.id')){
                $is_valid = 1;
            }

            if($is_valid == 0){
                //kembali ke login, harus dengan akun SI
                return redirect('kpsi/public/auth/google');
            }else{
                //beri akses sesuai dengan privilagenya
                $finduser = User::where('google_id', $user->id)->first();
                //dd($finduser);
                if($finduser){
       
                    Auth::login($finduser);

                    $request->session()->put('login','1');
                    $request->session()->put('email',$finduser->email);
                    $request->session()->put('name',$finduser->name);
                    $request->session()->put('is_admin',$finduser->is_admin);
                    $request->session()->put('nimnik',$finduser->nimnik);
      
                    //jika dosen == 1
                    if($is_valid == 1)
                        return redirect('/kpsi/public/dosen');
                    else
                        return redirect('/kpsi/public/mhs');
                }else{
                    $newUser = User::create([
                        'name' => $user->name,
                        'email' => $user->email,
                        'google_id'=> $user->id,
                        'avatar' => $user->avatar,
                        'is_admin'=> 0,
                        'is_aktif'=> 1
                    ]);
      
                    Auth::login($newUser);

                    $request->session()->put('login','1');
                    $request->session()->put('email',$user->email);
                    $request->session()->put('name',$user->name);
                    $request->session()->put('is_admin',0);
       
                    //return redirect()->back();

                    if($is_valid == 1)
                        return redirect('/kpsi/public/dosen');
                    else
                        return redirect('/kpsi/public/mhs');
                }
            }
        } catch (Exception $e) {
            //dd($e);
            return redirect('kpsi/public/auth/google');
        }
    }
}
