<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SuratKeterangan;
use App\Kp;
use App\PeriodeAktif;
use Session;
use Auth;
use DB;

class DosenController extends Controller
{
    public function index(){
        $findDosen = DB::table('users')
                        ->select('nimnik','name')
                        ->where('email','like','%staff.ukdw.ac.id')
                        ->get();
        $data['dosens'] = $findDosen;

        $findPraKp = DB::table('kps')
                        ->join('users as u','u.nimnik','=','kps.nim')
                        ->select('kps.id','kps.nim','u.name','kps.lembaga','kps.dokumen',
                            'kps.judul','kps.status_prakp','kps.id','kps.created_at')
                        ->where('kps.nik',Session::get('nimnik'))
                        ->whereIn('kps.status_prakp',[0,1,2])
                        ->get();
        $data['prakps'] = $findPraKp;        

        $findKp = DB::table('kps')
                        ->leftjoin('users as u','u.nimnik','=','kps.nim')
                        ->select('kps.id','kps.nim','u.name','kps.lembaga','kps.dokumen',
                            'kps.judul','kps.status_kp','kps.id','kps.created_at')
                        ->where('kps.nik',Session::get('nimnik'))
                        ->whereIn('kps.status_kp',[0,1,2])
                        ->get();
        $data['kps'] = $findKp;

    	return view('dosen/dash')->with($data);
    }

    public function suratKeterangan(){
        //$findSurat = SuratKeterangan::where('nim', '72170001')->get();
        $findSurat = DB::table('surat_keterangans as sk')
                        ->leftjoin('users as u','u.nimnik','=','sk.nim')
                        ->select('sk.id','sk.nim','sk.lembaga','sk.dokumen',
                            'u.name','sk.created_at',DB::raw('count(sk.tahun) as jml_pengajuan'))
                        ->groupBy('sk.id','sk.nim','sk.lembaga','sk.dokumen','u.name','sk.created_at')
                        ->where('sk.nim','72170001')
                        ->where('sk.is_verif',0)
                        ->get();
        $data['surats'] = $findSurat;

        return view('dosen/suratketerangan_view')->with($data);
    }

    public function suratketeranganpost(Request $request){
        $suratketerangan = new SuratKeterangan();

        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('dokumen');
        $tujuan_upload = 'suratketerangan';
        $file->move($tujuan_upload,$request->input('nim'). '-' . date("Y-m-d") .'.pdf');

        $data = array('semester'=>$request->input('semester'),
            'tahun'=> $request->input('tahun'),
            'nim' => $request->input('nim'),
            'lembaga' => $request->input('lembaga'),
            'pimpinan' => $request->input('pimpinan'),
            'no_telp' => $request->input('no_telp'),
            'alamat' => $request->input('alamat'),
            'fax' => $request->input('fax'),
            'dokumen' => $request->input('nim'). '-' . date("Y-m-d") .'.pdf'
            );
        $suratketerangan->saveSuratKeterangan($data);
        return redirect('/kpsi/public/dosen/suratketerangan')->with('success', 'New support ticket has been created! Wait sometime to get resolved');
    }

    public function suratKeteranganVerif($keputusan,$id){
        $findKeterangan = SuratKeterangan::where('id', $id)->first();
        $findKeterangan->is_verif = $keputusan;
        $findKeterangan->save();
        return redirect('/kpsi/public/dosen/suratketerangan')->with('success', 'New support ticket has been created! Wait sometime to get resolved');
    }

    public function kpPraKp(){
        //$findSurat = SuratKeterangan::where('nim', '72170001')->get();
        $findDosen = DB::table('users')
                        ->select('nimnik','name')
                        ->where('email','like','%staff.ukdw.ac.id')
                        ->get();
        $data['dosens'] = $findDosen;

        $findPraKp = DB::table('kps')
                        ->join('users as u','u.nimnik','=','kps.nim')
                        ->select('kps.id','kps.nim','u.name','kps.lembaga','kps.dokumen',
                            'kps.judul','kps.status_prakp','kps.id','kps.created_at')
                        ->where('nim','72170001')
                        ->where('kps.status_prakp',0)
                        ->get();
        $data['prakps'] = $findPraKp;        

        $findKp = DB::table('kps')
                        ->leftjoin('users as u','u.nimnik','=','kps.nim')
                        ->select('kps.id','kps.nim','u.name','kps.lembaga','kps.dokumen',
                            'kps.judul','kps.status_kp','kps.id','kps.created_at')
                        ->where('nim','72170001')
                        ->where('kps.status_kp',0)
                        ->get();
        $data['kps'] = $findKp;

        return view('dosen/setkpprakp_view')->with($data);
    }

    public function praKpVerif(Request $request){
        $findKp = Kp::where('id', $request->input('id'))->first();
        $findKp->nik = $request->input('nik');
        $findKp->status_prakp = $request->input('keputusan');
        $findKp->save();
        return redirect('/kpsi/public/dosen/kpprakp')->with('success', 'New support ticket has been created! Wait sometime to get resolved');
    }

    public function kpVerif(Request $request){
        $findKp = Kp::where('id', $request->input('id'))->first();
        $findKp->nik = $request->input('nik');
        $findKp->status_kp = $request->input('keputusan');
        $findKp->save();
        return redirect('/kpsi/public/dosen/kpprakp')->with('success', 'New support ticket has been created! Wait sometime to get resolved');
    }

    public function ujianKp(){
        //status ujian kp ada 3 yaitu
        //0 dosen sudah menyetujui
        //1 disetujui ujian
        //2 ditolak ujian

        $findAktifPeriode = DB::table('periode_aktifs')
                        ->select('tgl_awal','tgl_akhir')
                        ->where('is_aktif',1)
                        ->get()->first();

        $findKp = DB::table('kps')
                        ->leftjoin('users as u','u.nimnik','=','kps.nim')
                        ->leftjoin('users as dosen_bimbing','dosen_bimbing.nimnik','=','kps.nik')
                        ->select('kps.id','kps.nim','u.name as mhs','dosen_bimbing.name as pembimbing','penguji',
                            'kps.judul','kps.ruang','kps.jadwal_ujian')
                        ->where('kps.nim','72170001')
                        ->whereIn('kps.status_ujian',[0,1,2])
                        ->whereBetween('kps.updated_at',[$findAktifPeriode->tgl_awal,$findAktifPeriode->tgl_akhir])
                        ->get();
        $data['kps'] = $findKp;

        $findDosen = DB::table('users')
                        ->select('nimnik','name')
                        ->where('email','like','%staff.ukdw.ac.id')
                        ->get();
        $data['dosens'] = $findDosen;

        return view('dosen/ujiankp_view')->with($data);
    }

    public function updateUjianKp(Request $request){
        //status ujian kp ada 3 yaitu
        //0 dosen sudah menyetujui
        //1 disetujui ujian
        //2 ditolak ujian
        $findKp = Kp::where('id', $request->input('id'))->first();
        $findKp->penguji = $request->input('penguji');
        $findKp->status_ujian = $request->input('keputusan');
        $findKp->ruang = $request->input('ruang');
        $findKp->jadwal_ujian = $request->input('jadwal');
        $findKp->save();
        return redirect('/kpsi/public/dosen/ujianKp')->with('success', 'New exam schedule has been created! Wait sometime to get resolved');
    }

    public function periodeKp(){
        $findPeriode = PeriodeAktif::all();
        $data['periodes'] = $findPeriode;
        return view('dosen/kelolaperiode_view')->with($data);
    }

    public function updatePeriodeKp(Request $request){
        $findKp = PeriodeAktif::where('id', $request->input('id'))->first();
        $findKp->tgl_awal = $request->input('tgl_awal');
        $findKp->tgl_akhir = $request->input('tgl_akhir');
        $findKp->is_aktif = $request->input('is_aktif');
        $findKp->save();
        return redirect('/kpsi/public/dosen/periodekp')->with('success', 'New support ticket has been created! Wait sometime to get resolved');
    }

    public function periodeKpPost(Request $request){
        $periodeKp = new PeriodeAktif();
        $data = array('tahun'=>$request->input('tahun'),
            'semester'=>$request->input('semester'),
            'tgl_awal'=> $request->input('tgl_awal'),
            'tgl_akhir' => $request->input('tgl_akhir'),
            'is_aktif' => 1
            );
        $periodeKp->savePeriode($data);
        return redirect('/kpsi/public/dosen/periodekp')->with('success', 'New support ticket has been created! Wait sometime to get resolved');
    }

    public function listUjianKp(){
        //status ujian kp ada 3 yaitu
        //0 dosen sudah menyetujui
        //1 disetujui ujian
        //2 ditolak ujian

        $findAktifPeriode = DB::table('periode_aktifs')
                        ->select('tgl_awal','tgl_akhir')
                        ->where('is_aktif',1)
                        ->get()->first();

        $findKp = DB::table('kps')
                        ->leftjoin('users as u','u.nimnik','=','kps.nim')
                        ->leftjoin('users as dosen_uji','dosen_uji.nimnik','=','kps.penguji')
                        ->select('kps.id','kps.nim','u.name as mhs','dosen_uji.name as penguji',
                            'kps.judul','kps.ruang','kps.jadwal_ujian')
                        ->where('kps.nik',Session::get('nimnik'))
                        ->whereIn('kps.status_ujian',[0,1,2])
                        ->whereBetween('kps.updated_at',[$findAktifPeriode->tgl_awal,$findAktifPeriode->tgl_akhir])
                        ->get();
        $data['kps'] = $findKp;

        return view('dosen/listujiankp_view')->with($data);
    }
}
