<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SuratKeterangan;
use App\Kp;
use Auth;
use DB;
//use Illuminate\Support\Facades\Log;

class MhsController extends Controller
{
    public function index(){
    	$data['tasks'] = [
                [
                        'name' => 'Design New Dashboard',
                        'progress' => '87',
                        'color' => 'danger'
                ],
                [
                        'name' => 'Create Home Page',
                        'progress' => '76',
                        'color' => 'warning'
                ],
                [
                        'name' => 'Some Other Task',
                        'progress' => '32',
                        'color' => 'success'
                ],
                [
                        'name' => 'Start Building Website',
                        'progress' => '56',
                        'color' => 'info'
                ],
                [
                        'name' => 'Develop an Awesome Algorithm',
                        'progress' => '10',
                        'color' => 'success'
                ]
        ];
    	return view('mhs/dash')->with($data);
    }

    public function suratketerangan(){
    	$data['tasks'] = [
                [
                        'name' => 'Design New Dashboard',
                        'progress' => '87',
                        'color' => 'danger'
                ],
                [
                        'name' => 'Create Home Page',
                        'progress' => '76',
                        'color' => 'warning'
                ],
                [
                        'name' => 'Some Other Task',
                        'progress' => '32',
                        'color' => 'success'
                ],
                [
                        'name' => 'Start Building Website',
                        'progress' => '56',
                        'color' => 'info'
                ],
                [
                        'name' => 'Develop an Awesome Algorithm',
                        'progress' => '10',
                        'color' => 'success'
                ]
        ];

        $findSurat = SuratKeterangan::where('nim', '72170001')->get();
        $data['surats'] = $findSurat;

    	return view('mhs/suratketerangan_view')->with($data);
    }

    public function suratketeranganpost(Request $request){
    	$suratketerangan = new SuratKeterangan();

        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('dokumen');
        $tujuan_upload = 'suratketerangan';
        $file->move($tujuan_upload,$request->input('nim'). '-' . date("Y-m-d") .'.pdf');

    	$data = array('semester'=>$request->input('semester'),
            'tahun'=> $request->input('tahun'),
            'nim' => $request->input('nim'),
            'lembaga' => $request->input('lembaga'),
            'pimpinan' => $request->input('pimpinan'),
            'no_telp' => $request->input('no_telp'),
            'alamat' => $request->input('alamat'),
            'fax' => $request->input('fax'),
            'dokumen' => $request->input('nim'). '-' . date("Y-m-d") .'.pdf'
            );
        $suratketerangan->saveSuratKeterangan($data);
        return redirect('/kpsi/public/mhs/suratketerangan')->with('success', 'New support ticket has been created! Wait sometime to get resolved');
    }

    public function logOutGoogle(Request $request){
        Auth::logout();
        $request->session()->forget('login');
        return redirect('/kpsi/public/');
    }

    //pengajuan pra kp
    public function praKp(){
        //$findPraKp = Kp::where('nim', '72170001')->get();
        $findPraKp = DB::table('kps')
                        ->leftjoin('users as u','u.nimnik','=','kps.nik')
                        ->select('kps.id','kps.semester','kps.tahun','kps.nim','kps.lembaga',
                            'kps.pimpinan','kps.no_telp','kps.alamat','kps.fax','kps.dokumen',
                            'kps.judul','kps.status_prakp','kps.id','u.name as dosen','kps.created_at')
                        ->where('nim','72170001')
                        ->whereIn('kps.status_prakp',[0,1,2])
                        ->get();
        $data['kps'] = $findPraKp;

        return view('mhs/prakp_view')->with($data);
    }

    public function praKpPost(Request $request){
        $kp = new Kp();

        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('dokumen');
        $tujuan_upload = 'prakp';
        $file->move($tujuan_upload,$request->input('nim'). '-' . date("Y-m-d") .'.pdf');

        $data = array('semester'=>$request->input('semester'),
            'tahun'=> $request->input('tahun'),
            'nim' => $request->input('nim'),
            'lembaga' => $request->input('lembaga'),
            'pimpinan' => $request->input('pimpinan'),
            'no_telp' => $request->input('no_telp'),
            'alamat' => $request->input('alamat'),
            'fax' => $request->input('fax'),
            'dokumen' => $request->input('nim'). '-' . date("Y-m-d") .'.pdf',
            'judul' => $request->input('judul'),
            'status_prakp' => 0,
            'tools' => $request->input('tools'),
            'spesifikasi' => $request->input('spesifikasi')
            );
        $kp->savePraKp($data);
        return redirect('/kpsi/public/mhs/prakp')->with('success', 'Pengajuan Pra KP Anda sudah masuk! Tunggu Konfirmasi dari Koordinator');
    }

    //pengajuan kp
    public function kp(){
        $findPraKp = DB::table('kps')
                        ->join('users as u','u.nimnik','=','kps.nik')
                        ->select('kps.id','kps.semester','kps.tahun','kps.nim','kps.lembaga',
                            'kps.pimpinan','kps.no_telp','kps.alamat','kps.fax','kps.dokumen',
                            'kps.judul','kps.status_prakp','kps.id','u.name as dosen','kps.created_at')
                        ->where('nim','72170001')
                        ->whereIn('kps.status_prakp',[0,1,2])
                        ->get();
        $data['prakps'] = $findPraKp;

        $findKp = DB::table('kps')
                        ->leftjoin('users as u','u.nimnik','=','kps.nik')
                        ->select('kps.id','kps.semester','kps.tahun','kps.nim','kps.lembaga',
                            'kps.pimpinan','kps.no_telp','kps.alamat','kps.fax','kps.dokumen',
                            'kps.judul','kps.status_kp','kps.id','u.name as dosen','kps.created_at')
                        ->where('nim','72170001')
                        ->whereIn('kps.status_kp',[0,1,2])
                        ->get();
        $data['kps'] = $findKp;
        //dd($findKp);

        return view('mhs/kp_view')->with($data);
    }

    public function kpPost(Request $request){
        $kp = new Kp();

        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('dokumen');
        $tujuan_upload = 'kp';
        $file->move($tujuan_upload,$request->input('nim'). '-' . date("Y-m-d") .'.pdf');

        $data = array('semester'=>$request->input('semester'),
            'tahun'=> $request->input('tahun'),
            'nim' => $request->input('nim'),
            'lembaga' => $request->input('lembaga'),
            'pimpinan' => $request->input('pimpinan'),
            'no_telp' => $request->input('no_telp'),
            'alamat' => $request->input('alamat'),
            'fax' => $request->input('fax'),
            'dokumen' => $request->input('nim'). '-' . date("Y-m-d") .'.pdf',
            'judul' => $request->input('judul'),
            'status_kp' => 0,
            'tools' => $request->input('tools'),
            'spesifikasi' => $request->input('spesifikasi')
            );
        $kp->saveKp($data);
        return redirect('/kpsi/public/mhs/kp')->with('success', 'Pengajuan KP Anda sudah masuk! Tunggu Konfirmasi dari Koordinator');
    }
}
