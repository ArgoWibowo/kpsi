<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuratKeterangan extends Model
{
    protected $fillable = ['semester', 'tahun', 'nim','lembaga','pimpinan',
    'no_telp','alamat','fax','dokumen','is_verif'];

    public function saveSuratKeterangan($data)
	{
		$this->semester = $data['semester'];
        $this->tahun = $data['tahun'];
        $this->nim = $data['nim'];
        $this->lembaga = $data['lembaga'];
        $this->pimpinan = $data['pimpinan'];
        $this->no_telp = $data['no_telp'];
        $this->alamat = $data['alamat'];
        $this->fax = $data['fax'];
        $this->dokumen = $data['dokumen'];
        $this->save();
        return 1;
	}
}
