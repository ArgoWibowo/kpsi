<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeriodeAktif extends Model
{
    protected $fillable = ['tahun', 'semester', 'tgl_awal', 'tgl_akhir','is_aktif'];

    public function savePeriode($data)
	{
		$this->tahun = $data['tahun'];
        $this->semester = $data['semester'];
        $this->tgl_awal = $data['tgl_awal'];
        $this->tgl_akhir = $data['tgl_akhir'];
        $this->is_aktif = $data['is_aktif'];
        $this->save();
        return 1;
	}
}
