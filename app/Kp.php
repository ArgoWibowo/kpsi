<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kp extends Model
{
	//status prakp ada 2
    //0=belum ada kabar
	//1=diterima pra kp
	//2=ditolak pra kp
    //0=belum ada kabar
	//1=diterima kp
	//2=diteditolakrima kp
    protected $fillable = ['semester', 'tahun', 'nim','lembaga','pimpinan',
    'no_telp','alamat','fax','dokumen','judul','status_kp','status_prakp',
    'ruang','nik','tools','spesifikasi','penguji','jadwal_ujian','status_ujian'];

    public function savePraKp($data)
	{
		$this->semester = $data['semester'];
        $this->tahun = $data['tahun'];
        $this->nim = $data['nim'];
        $this->lembaga = $data['lembaga'];
        $this->pimpinan = $data['pimpinan'];
        $this->no_telp = $data['no_telp'];
        $this->alamat = $data['alamat'];
        $this->fax = $data['fax'];
        $this->dokumen = $data['dokumen'];
        $this->judul = $data['judul'];
        $this->status_prakp = $data['status_prakp'];
        $this->tools = $data['tools'];
        $this->spesifikasi = $data['spesifikasi'];
        $this->save();
        return 1;
	}

    public function saveKp($data)
    {
        $this->semester = $data['semester'];
        $this->tahun = $data['tahun'];
        $this->nim = $data['nim'];
        $this->lembaga = $data['lembaga'];
        $this->pimpinan = $data['pimpinan'];
        $this->no_telp = $data['no_telp'];
        $this->alamat = $data['alamat'];
        $this->fax = $data['fax'];
        $this->dokumen = $data['dokumen'];
        $this->judul = $data['judul'];
        $this->status_kp = $data['status_kp'];
        $this->tools = $data['tools'];
        $this->spesifikasi = $data['spesifikasi'];
        $this->save();
        return 1;
    }

    public function updateKp($data)
    {
        $this->semester = $data['semester'];
        $this->tahun = $data['tahun'];
        $this->nim = $data['nim'];
        $this->lembaga = $data['lembaga'];
        $this->pimpinan = $data['pimpinan'];
        $this->no_telp = $data['no_telp'];
        $this->alamat = $data['alamat'];
        $this->fax = $data['fax'];
        $this->dokumen = $data['dokumen'];
        $this->judul = $data['judul'];
        $this->status_kp = $data['status_kp'];
        $this->tools = $data['tools'];
        $this->spesifikasi = $data['spesifikasi'];
        $this->save();
        return 1;
    }
}
