-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 06, 2019 at 08:10 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kpsi`
--

-- --------------------------------------------------------

--
-- Table structure for table `kps`
--

CREATE TABLE `kps` (
  `id` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `nim` varchar(20) NOT NULL,
  `lembaga` varchar(255) NOT NULL,
  `pimpinan` varchar(255) NOT NULL,
  `no_telp` varchar(15) DEFAULT NULL,
  `alamat` text NOT NULL,
  `fax` varchar(15) DEFAULT NULL,
  `dokumen` varchar(255) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `status_kp` int(11) DEFAULT NULL,
  `status_prakp` int(11) DEFAULT NULL,
  `ruang` varchar(255) DEFAULT NULL,
  `nik` varchar(20) DEFAULT NULL,
  `tools` text NOT NULL,
  `spesifikasi` text NOT NULL,
  `penguji` varchar(20) DEFAULT NULL,
  `status_ujian` int(11) DEFAULT NULL,
  `jadwal_ujian` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kps`
--

INSERT INTO `kps` (`id`, `semester`, `tahun`, `nim`, `lembaga`, `pimpinan`, `no_telp`, `alamat`, `fax`, `dokumen`, `judul`, `status_kp`, `status_prakp`, `ruang`, `nik`, `tools`, `spesifikasi`, `penguji`, `status_ujian`, `jadwal_ujian`, `created_at`, `updated_at`) VALUES
(1, 1, 2019, '72170001', 'kljklj', 'klj', 'kljkl', 'lkj', 'lkj', '72170001-2019-08-29.pdf', 'Perancangan SI dengan menggunakan metode SDLC', 2, 1, NULL, '0516118902', 'kjlk', 'jljlkj', NULL, NULL, NULL, '2019-08-29 01:31:08', '2019-08-29 15:05:45'),
(2, 1, 2019, '72170001', '789789', '789789', '789789', '89798', '8789', '72170001-2019-08-29.pdf', '90898', NULL, 1, NULL, '0516118902', '987897', '98789789', NULL, NULL, NULL, '2019-08-29 02:41:19', '2019-08-29 15:03:35'),
(3, 1, 2019, '72170001', '987', '98789', '7897', '97987', '89789', '9897-2019-08-29.pdf', '98798', NULL, 2, NULL, '0516118902', '798', '7987', NULL, NULL, NULL, '2019-08-29 03:27:37', '2019-08-29 15:03:46'),
(4, 1, 2018, '72170001', '8787', '8787', '8787', '8787', '8787', '72170001-2019-08-29.pdf', '87878', 1, 1, 'Gateway', '0516118902', '78787', '8787', '0513118903', 1, '2019-08-28 18:30:00', '2019-08-29 03:29:04', '2019-08-31 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `periode_aktifs`
--

CREATE TABLE `periode_aktifs` (
  `id` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `tgl_awal` date NOT NULL,
  `tgl_akhir` date NOT NULL,
  `is_aktif` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `periode_aktifs`
--

INSERT INTO `periode_aktifs` (`id`, `tahun`, `semester`, `tgl_awal`, `tgl_akhir`, `is_aktif`, `created_at`, `updated_at`) VALUES
(1, 2019, 1, '2019-08-07', '2019-12-31', 1, '2019-09-02 08:47:43', '2019-09-05 06:18:39');

-- --------------------------------------------------------

--
-- Table structure for table `surat_keterangans`
--

CREATE TABLE `surat_keterangans` (
  `id` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `nim` varchar(20) NOT NULL,
  `lembaga` varchar(255) NOT NULL,
  `pimpinan` varchar(255) NOT NULL,
  `no_telp` varchar(15) DEFAULT NULL,
  `alamat` text NOT NULL,
  `fax` varchar(15) DEFAULT NULL,
  `dokumen` varchar(255) NOT NULL,
  `is_verif` int(11) DEFAULT '0',
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `surat_keterangans`
--

INSERT INTO `surat_keterangans` (`id`, `semester`, `tahun`, `nim`, `lembaga`, `pimpinan`, `no_telp`, `alamat`, `fax`, `dokumen`, `is_verif`, `updated_at`, `created_at`) VALUES
(3, 1, 2019, '72170001', 'Telkomsel', 'Pak Argo', '082837897', 'Alamat Telkomsel', '72170001-26-08-', '72170001-26-08-2019.pdf', 1, '2019-08-29 05:53:07', '2019-08-25 16:14:21'),
(4, 1, 2019, '72170001', 'universitas kristen duta wacana', 'Jong Jek Siang', '08883475845', 'Jl Dr Wahidin No 5-25', '09394589345', '72170001-28-08-2019.pdf', 2, '2019-08-29 05:57:19', '2019-08-27 23:29:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `nimnik` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_admin` int(11) NOT NULL,
  `is_aktif` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nimnik`, `name`, `email`, `remember_token`, `google_id`, `avatar`, `is_admin`, `is_aktif`, `created_at`, `updated_at`) VALUES
(3, '0516118902', 'Argo Wibowo, S.T., M.T', 'argo@staff.ukdw.ac.id', 'T7MBCi5TRwEX3BbDte9BUp9u0L6PDIQuENSFe4IBLFcuYB0AA4TdTT7nJrhv', '107676380614815412891', 'https://lh3.googleusercontent.com/a-/AAuE7mBrI3jmlffe8nREEizNnji4KgcFaD2P47YdTMfYBQ', 1, 1, '2019-08-24 20:24:37', '2019-08-24 20:24:37'),
(4, '72170001', 'Argo Uchiha', 'argo@si.ukdw.ac.id', 'lZLRJOLztiuDHgO4QF77CnHnZ52xyU8zt0Eo0QxXevThL3aaVatzR0o4Wwni', '101225015224896394163', NULL, 0, 1, '2019-08-25 05:43:44', '2019-08-25 05:43:44'),
(5, '0513118903', 'Halim Budi Santoso, S.T., M.T', 'hbudi@staff.ukdw.ac.id', 'pam0146cRHwQ4BrGobjB5Q3ul96vonym84rlS89VHIoWJE5Y6qhEO7dYCXNL', '107676380614815412891', 'https://lh3.googleusercontent.com/a-/AAuE7mBrI3jmlffe8nREEizNnji4KgcFaD2P47YdTMfYBQ', 0, 1, '2019-08-24 20:24:37', '2019-08-24 20:24:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kps`
--
ALTER TABLE `kps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `periode_aktifs`
--
ALTER TABLE `periode_aktifs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `surat_keterangans`
--
ALTER TABLE `surat_keterangans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kps`
--
ALTER TABLE `kps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `periode_aktifs`
--
ALTER TABLE `periode_aktifs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `surat_keterangans`
--
ALTER TABLE `surat_keterangans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
