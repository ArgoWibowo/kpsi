@extends('dosen.dosen_template')

@section('content')

<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Daftar Pengajuan Pra KP</h3>
        </div>
        <!-- form start -->
        <form role="form">
            <!-- /.box-header -->
            <input type="hidden" value="{{csrf_token()}}" name="_token" />
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Verifikasi</th>
                  <th>NIM</th>
                  <th>Nama</th>
                  <th>Dosen Pembimbing</th>
                  <th>Lembaga</th>
                  <th>Tgl</th>
                </tr>
                @foreach($prakps as $surat)
                <tr>
                  <td>{{$loop->index + 1}}</td>
                  <td>
                    <button onClick="setPraKp(1,{{$surat->id}},'{{url('kpsi/public/dosen/prakpverif')}}')" class="btn btn-success fa fa-check"></button>
                    <button onClick="setPraKp(2,{{$surat->id}},'{{url('kpsi/public/dosen/prakpverif')}}')" class="btn btn-danger fa fa-close"></button>
                  </td>
                  <td>{{$surat->nim}}</td>
                  <td>{{$surat->name}}</td>
                  <td>
                    <select class="form-control" name="dosenpra-{{$surat->id}}" id="dosenpra-{{$surat->id}}">
                      @foreach($dosens as $dosen)
                      <option value="{{$dosen->nimnik}}">{{$dosen->name}}</option>
                      @endforeach
                    </select>
                  </td>
                  <td>{{$surat->lembaga}}</td>
                  <td>{{$surat->created_at}}</td>
                </tr>
                @endforeach
              </tbody></table>
            </div>
            <!-- /.box-body -->
        </form>
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Daftar Pengajuan KP</h3>
        </div>
        <!-- form start -->
        <form role="form">
            <!-- /.box-header -->
            <input type="hidden" value="{{csrf_token()}}" name="_token" />
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Verifikasi</th>
                  <th>NIM</th>
                  <th>Nama</th>
                  <th>Dosen Pembimbing</th>
                  <th>Lembaga</th>
                  <th>Tgl</th>
                </tr>
                @foreach($kps as $surat)
                <tr>
                  <td>{{$loop->index + 1}}</td>
                  <td>
                    <button onClick="setKp(1,{{$surat->id}},'{{url('kpsi/public/dosen/kpverif')}}')" class="btn btn-success fa fa-check"></button>
                    <button onClick="setKp(2,{{$surat->id}},'{{url('kpsi/public/dosen/kpverif')}}')" class="btn btn-danger fa fa-close"></button>
                  </td>
                  <td>{{$surat->nim}}</td>
                  <td>{{$surat->name}}</td>
                  <td>
                    <select class="form-control" name="dosen-{{$surat->id}}" id="dosen-{{$surat->id}}">
                      @foreach($dosens as $dosen)
                      <option value="{{$dosen->nimnik}}">{{$dosen->name}}</option>
                      @endforeach
                    </select>
                  </td>
                  </td>
                  <td>{{$surat->lembaga}}</td>
                  <td>{{$surat->created_at}}</td>
                </tr>
                @endforeach
              </tbody></table>
            </div>
            <!-- /.box-body -->
        </form>
      </div>
      <!-- /.box -->
    </div>
</div>

<script>
  function setPraKp(status,id,url) {
    var e = document.getElementById("dosenpra-"+id);
    var strUser = e.options[e.selectedIndex].value;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        window.location.reload(true);
      }
    };
    xhttp.open("POST", url, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("keputusan="+status+"&nik="+strUser+"&id="+id);
  }

  function setKp(status,id,url) {
    var e = document.getElementById("dosen-"+id);
    var strUser = e.options[e.selectedIndex].value;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        window.location.reload(true);
      }
    };
    xhttp.open("POST", url, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("keputusan="+status+"&nik="+strUser+"&id="+id);
  }
</script>
@endsection