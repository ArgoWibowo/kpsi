@extends('dosen.dosen_template')

@section('content')

<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Daftar Ujian KP - {{Session::get('name')}} - {{Session::get('nimnik')}}</h3>
        </div>
        <!-- form start -->
        <form role="form">
            <!-- /.box-header -->
            <input type="hidden" value="{{csrf_token()}}" name="_token" />
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody>
                <tr>
                  <th style="width: 10px">#</th>
                  <th style="width: 15%">Tgl Ujian</th>
                  <th>Ruang</th>
                  <th>NIM</th>
                  <th>Nama</th>
                  <th>Judul</th>
                  <th>Dosen Penguji</th>
                </tr>
                @foreach($kps as $surat)
                <tr>
                  <td>{{$loop->index + 1}}</td>
                  <td>{{$surat->jadwal_ujian}}</td>
                  <td>{{$surat->ruang}}</td>
                  <td>{{$surat->nim}}</td>
                  <td>{{$surat->mhs}}</td>
                  <td>{{$surat->judul}}</td>
                  <td>{{$surat->penguji}}</td>
                </tr>
                @endforeach
              </tbody></table>
            </div>
            <!-- /.box-body -->
        </form>
      </div>
      <!-- /.box -->
    </div>
</div>

<script>
  function setUjianKp(status,id,url) {
    var e = document.getElementById("ruang-"+id);
    var ruang = e.options[e.selectedIndex].value;
    var tp = document.getElementById("timepicker-"+id).value;
    var dp = document.getElementById("datepicker-"+id).value;
    var jadwal = dp + " " + tp + ":00";

    var e = document.getElementById("penguji-"+id);
    var penguji = e.options[e.selectedIndex].value;

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        window.location.reload(true);
      }
    };
    xhttp.open("POST", url, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("keputusan="+status+"&penguji="+penguji+"&id="+id+"&ruang="+ruang+"&jadwal="+jadwal);
  }
</script>
@endsection