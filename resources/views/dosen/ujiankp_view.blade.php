@extends('dosen.dosen_template')

@section('content')

<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Daftar Pengajuan Ujian KP</h3>
        </div>
        <!-- form start -->
        <form role="form">
            <!-- /.box-header -->
            <input type="hidden" value="{{csrf_token()}}" name="_token" />
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Verifikasi</th>
                  <th style="width: 15%">Tgl Ujian</th>
                  <th>Ruang</th>
                  <th>NIM</th>
                  <th>Nama</th>
                  <th>Judul</th>
                  <th>Dosen Pembimbing</th>
                  <th>Dosen Penguji</th>
                </tr>
                @foreach($kps as $surat)
                <tr>
                  <td>{{$loop->index + 1}}</td>
                  <td>
                    <button onClick="setUjianKp(1,{{$surat->id}},'{{url('kpsi/public/dosen/updateujiankp')}}')" class="btn btn-success fa fa-check"></button>
                    <button onClick="setUjianKp(2,{{$surat->id}},'{{url('kpsi/public/dosen/updateujiankp')}}')" class="btn btn-danger fa fa-close"></button>
                  </td>
                  <td>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                        <i class="fa fa-clock-o"></i>
                      </div>
                      <input type="text" class="datepicker form-control" style="padding: 12px" id="datepicker-{{$surat->id}}" name="datepicker-{{$surat->id}}"
                      @if($surat->jadwal_ujian != null) value="{{substr($surat->jadwal_ujian,0,10)}}" @endif>
                      <input type="text" class="form-control timepicker" id="timepicker-{{$surat->id}}" name="timepicker-{{$surat->id}}"
                      @if($surat->jadwal_ujian != null) value="{{substr($surat->jadwal_ujian,11,5)}}" @endif>
                    </div>
                  </td>
                  <td>
                    <select class="form-control" name="ruang-{{$surat->id}}" id="ruang-{{$surat->id}}">
                      <option value="Java" @if($surat->ruang == 'Java') selected @endif>Java</option>
                      <option value="Kernel" @if($surat->ruang == 'Kernel') selected @endif>Kernel</option>
                      <option value="Gateway" @if($surat->ruang == 'Gateway') selected @endif>Gateway</option>
                      <option value="Firewall" @if($surat->ruang == 'Firewall') selected @endif>Firewall</option>
                    </select>
                  </td>
                  <td>{{$surat->nim}}</td>
                  <td>{{$surat->mhs}}</td>
                  <td>{{$surat->judul}}</td>
                  <td>{{$surat->pembimbing}}</td>
                  <td>
                    <select class="form-control" name="penguji-{{$surat->id}}" id="penguji-{{$surat->id}}">
                      @foreach($dosens as $dosen)
                      <option value="{{$dosen->nimnik}}" 
                        @if($surat->penguji == $dosen->nimnik) selected @endif>
                        {{$dosen->name}}</option>
                      @endforeach
                    </select>
                  </td>
                </tr>
                @endforeach
              </tbody></table>
            </div>
            <!-- /.box-body -->
        </form>
      </div>
      <!-- /.box -->
    </div>
</div>

<script>
  function setUjianKp(status,id,url) {
    var e = document.getElementById("ruang-"+id);
    var ruang = e.options[e.selectedIndex].value;
    var tp = document.getElementById("timepicker-"+id).value;
    var dp = document.getElementById("datepicker-"+id).value;
    var jadwal = dp + " " + tp + ":00";

    var e = document.getElementById("penguji-"+id);
    var penguji = e.options[e.selectedIndex].value;

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        window.location.reload(true);
      }
    };
    xhttp.open("POST", url, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("keputusan="+status+"&penguji="+penguji+"&id="+id+"&ruang="+ruang+"&jadwal="+jadwal);
  }
</script>
@endsection