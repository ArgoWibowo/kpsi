@extends('dosen.dosen_template')

@section('content')

<div class="row">
    <!-- left column -->
    <div class="col-md-6">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Data Periode KP</h3>
        </div>
        <!-- form start -->
        <form role="form" method="post" enctype="multipart/form-data" action="{{url('kpsi/public/dosen/periodekppost')}}">
          <div class="box-body">
                <input type="hidden" value="{{csrf_token()}}" name="_token" />
            <div class="form-group">
                <label for="semester">Semester:</label>
                <select style="width: 25%" class="form-control" name="semester" id="semester">
                  <option value="1">Gasal</option>
                  <option value="2">Genap</option>
                </select>
            </div>
            <div class="form-group">
                <label for="title">Tahun:</label>
                <input type="number" style="width: 25%" class="form-control" name="tahun"/>
            </div>
            <div class="form-group">
                <label for="title">Tanggal Mulai:</label>
                <input style="width: 25%" type="text" class="datepicker form-control" style="padding: 12px" id="tgl_awal" name="tgl_awal"/>
            </div>
            <div class="form-group">
                <label for="title">Tanggal Selesai:</label>
                <input style="width: 25%" type="text" class="datepicker form-control" style="padding: 12px" id="tgl_akhir" name="tgl_akhir"/>
            </div>
            
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Daftar Periode KP</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <table class="table table-striped">
            <tbody>
            <tr>
              <th style="width: 10px">#</th>
              <th style="width: 100px">Update</th>
              <th style="width: 20%">Nama Periode</th>
              <th>Tgl Awal</th>
              <th>Tgl Akhir</th>
            </tr>
            @foreach($periodes as $kp)
            <tr>
              <td>{{$loop->index + 1}}</td>
              <td>
                <button onClick="setPeriodeKp(1,{{$kp->id}},'{{url('kpsi/public/dosen/updateperiodekp')}}')" class="btn btn-success fa fa-check"></button>
                <button onClick="setPeriodeKp(0,{{$kp->id}},'{{url('kpsi/public/dosen/updateperiodekp')}}')" class="btn btn-danger fa fa-close"></button>
              </td>
              <td>{{$kp->tahun}} - @if($kp->semester == 1) GASAL @else GENAP @endif - @if($kp->is_aktif == 1) AKTIF @else NonAktif @endif</td>
              <td>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="datepicker form-control" style="padding: 12px" id="tgl_awal-{{$kp->id}}" name="tgl_awal-{{$kp->id}}"
                  @if($kp->tgl_awal != null) value="{{$kp->tgl_awal}}" @endif>
                </div>
              </td>
              <td>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="datepicker form-control" style="padding: 12px" id="tgl_akhir-{{$kp->id}}" name="tgl_akhir-{{$kp->id}}"
                  @if($kp->tgl_akhir != null) value="{{$kp->tgl_akhir}}" @endif>
                </div>
              </td>
            </tr>
            @endforeach
          </tbody></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
</div>

<script>
  function setPeriodeKp(status,id,url) {
    var tgl_awal = document.getElementById("tgl_awal-"+id).value;
    var tgl_akhir = document.getElementById("tgl_akhir-"+id).value;

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        window.location.reload(true);
      }
    };
    xhttp.open("POST", url, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("is_aktif="+status+"&tgl_awal="+tgl_awal+"&id="+id+"&tgl_akhir="+tgl_akhir);
  }
</script>
@endsection