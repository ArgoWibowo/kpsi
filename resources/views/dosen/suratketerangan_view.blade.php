@extends('dosen.dosen_template')

@section('content')

<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Daftar Pengajuan Surat Keterangan KP</h3>
        </div>
        <!-- form start -->
        <form role="form" method="post" enctype="multipart/form-data" action="{{url('kpsi/public/mhs/suratketeranganpost')}}">
            <!-- /.box-header -->
            <input type="hidden" value="{{csrf_token()}}" name="_token" />
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Verifikasi</th>
                  <th>NIM</th>
                  <th>Nama</th>
                  <th>Jumlah Pengajuan</th>
                  <th>Lembaga</th>
                  <th>Tgl</th>
                </tr>
                @foreach($surats as $surat)
                <tr>
                  <td>{{$loop->index + 1}}</td>
                  <td>
                    <a href="{{url('kpsi/public/dosen/suratketerangan/1/'.$surat->id)}}" class="btn btn-success fa fa-check"></a>
                    <a href="{{url('kpsi/public/dosen/suratketerangan/2/'.$surat->id)}}" class="btn btn-danger fa fa-close"></a>
                  </td>
                  <td>{{$surat->nim}}</td>
                  <td>{{$surat->name}}</td>
                  <td>{{$surat->jml_pengajuan}}</td>
                  <td>{{$surat->lembaga}}</td>
                  <td>{{$surat->created_at}}</td>
                </tr>
                @endforeach
              </tbody></table>
            </div>
            <!-- /.box-body -->
        </form>
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->
    <div class="col-md-6">
      
      <!-- /.box -->
    </div>
</div>
@endsection