<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset('kpsi/public/lte/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>User</p>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="{{url('kpsi/public/mhs/suratketerangan')}}"><i class="fa fa-link"></i> <span>Pengajuan Surat Keterangan</span></a></li>
        <li><a href="{{url('kpsi/public/mhs/prakp')}}"><i class="fa fa-link"></i> <span>Pengajuan Pra KP</span></a></li>
        <li><a href="{{url('kpsi/public/mhs/kp')}}"><i class="fa fa-link"></i> <span>Pengajuan KP</span></a></li>
        <li><a href="#"><i class="fa fa-link"></i> <span>Pengajuan Ujian</span></a></li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>