@extends('mhs.mhs_template')

@section('content')

<div class="row">
    <!-- left column -->
    <div class="col-md-6">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Data Pengajuan KP</h3>
        </div>
        <!-- form start -->
        <form role="form" method="post" enctype="multipart/form-data" action="{{url('kpsi/public/mhs/kppost')}}">
          <div class="box-body">
                <input type="hidden" value="{{csrf_token()}}" name="_token" />
            <div class="form-group">
                <label for="semester">Semester:</label>
                <select style="width: 25%" class="form-control" name="semester" id="semester">
                  <option value="1">Gasal</option>
                  <option value="2">Genap</option>
                </select>
            </div>
            <div class="form-group">
                <label for="title">Tahun:</label>
                <input type="number" style="width: 25%" class="form-control" name="tahun"/>
            </div>
            <div class="form-group">
                <label for="title">NIM:</label>
                <input type="text" style="width: 25%" class="form-control" name="nim"/>
            </div>
            <div class="form-group">
                <label for="title">Judul Kerja Praktik:</label>
                <textarea class="form-control" name="judul" rows="3" placeholder="Judul ..."></textarea>
            </div>
            <div class="form-group">
                <label for="title">Tools:</label>
                <textarea class="form-control" name="tools" rows="3" placeholder="Tools ..."></textarea>
            </div>
            <div class="form-group">
                <label for="title">Spesifikasi Perangkat Lunak / Pekerjaan KP:</label>
                <textarea class="form-control" name="spesifikasi" rows="3" placeholder="Spesifikasi ..."></textarea>
            </div>
            <div class="form-group">
                <label for="title">Lembaga:</label>
                <input type="text" class="form-control" name="lembaga"/>
            </div>
            <div class="form-group">
                <label for="title">Pimpinan:</label>
                <input type="text" class="form-control" name="pimpinan"/>
            </div>
            <div class="form-group">
                <label for="title">No. Telp:</label>
                <input type="text" style="width: 50%" class="form-control" name="no_telp"/>
            </div>
            <div class="form-group">
                <label for="title">Alamat:</label>
                <textarea class="form-control" name="alamat" rows="3" placeholder="Alamat ..."></textarea>
            </div>
            <div class="form-group">
                <label for="title">Fax:</label>
                <input type="text" style="width: 50%" class="form-control" name="fax"/>
            </div>
            <div class="form-group">
                <label for="title">Dokumen (PDF Scan):</label>
                <input type="file" class="form-control" name="dokumen"/>
            </div>
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Daftar Pengajuan Pra KP</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <table class="table table-striped">
            <tbody>
            <tr>
              <th style="width: 10px">#</th>
              <th>Judul</th>
              <th>Pembimbing</th>
              <th>Tgl</th>
              <th style="width: 40px">Disetujui</th>
            </tr>
            @foreach($prakps as $kp)
            <tr>
              <td>{{$loop->index + 1}}</td>
              <td>{{$kp->judul}}</td>
              <td bgcolor="#F4F100">{{$kp->dosen}}</td>
              <td>{{$kp->created_at}}</td>
              @if($kp->status_prakp == 2)
              <td bgcolor="#FF0000">Tidak</td>
              @else
              <td bgcolor="#00FF00">Ya</td>
              @endif
            </tr>
            @endforeach
          </tbody></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Daftar Pengajuan KP</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <table class="table table-striped">
            <tbody>
            <tr>
              <th style="width: 10px">#</th>
              <th>Judul</th>
              <th>Pembimbing</th>
              <th>Tgl</th>
              <th style="width: 40px">Disetujui</th>
            </tr>
            @foreach($kps as $kp)
            <tr>
              <td>{{$loop->index + 1}}</td>
              <td>{{$kp->judul}}</td>
              <td bgcolor="#F4F100">{{$kp->dosen}}</td>
              <td>{{$kp->created_at}}</td>
              @if($kp->status_kp == 2)
              <td bgcolor="#FF0000">Tidak</td>
              @elseif($kp->status_kp == 1)
              <td bgcolor="#00FF00">Ya</td>
              @else
              <td bgcolor="#E8DEDE">-</td>
              @endif
            </tr>
            @endforeach
          </tbody></table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
</div>
@endsection